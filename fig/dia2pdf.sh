#!/bin/bash

for filename in $(ls *.dia); do
    name=${filename%.*}
    dia -e ${name}.svg ${name}.dia
    inkscape -z -A ${name}.pdf ${name}.svg
    rm ${name}.svg
done
