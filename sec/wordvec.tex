\subsection{Word Vectors}

As already mentioned, an ANN is a connected network of artificial neurons which
in turn are a kind of mathematical functions. Therefore, it is clear that ANNs
only work with numbers. So, to use neural networks to build a {\em text}
classifier (such as sentiment analysis), a way is needed to represent text as
numbers. One naive way would be to use the ASCII values of the characters that
constitute the text but it does not take too long to realize that, except for
sorting the text and changing the case, the ASCII values of the text are not
really useful. Anyway, sorting and changing case are not a part of NLP either.

Another way of representing text as numbers is by using various count
statistics. For example, a text could be represented as a vector of numbers
where each element is the count (frequency) of the occurrence of a word in the
vocabulary. The dimensionality of such a vector would be equal to the size of
the vocabulary. If there are 100,000 words in the vocabulary, each document can
be represented by a vector of size 100,000. Obviously, the vector would be very
sparse, i.e., most of the elements would be zero. Term Frequency--Inverse
Document Frequency (TF-IDF) is a popular way of weighting the importance of a
word in a document. Term frequency is the frequency of the occurrence of a term
in a document, and inverse document frequency is the factor which diminishes the
weight of terms that occur very frequently in the document set and increases the
weight of terms that occur rarely. This representation is used by the BOW model.
Another similar method is to represent the words as {\em one-hot} vectors, which
basically is a very sparse vector having dimensionality equal to the size of the
vocabulary and having all its elements zero except at the index of that word in
the vocabulary.

Consider the following small corpus:

\begin{quote}
  "A boy can do everything for girls."
\end{quote}

This corpus has a vocabulary size of 7. Now if the words in the vocabulary are
arranged in the given order, then the one-hot vectors would be:
\begin{align}
  \text{A} &= \text{[1 0 0 0 0 0 0]} \nonumber\\
  \text{boy} &= \text{[0 1 0 0 0 0 0]} \nonumber\\
  \text{can} &= \text{[0 0 1 0 0 0 0]} \nonumber\\
  \text{do} &= \text{[0 0 0 1 0 0 0]} \nonumber\\
  \text{everything} &= \text{[0 0 0 0 1 0 0]} \nonumber\\
  \text{for} &= \text{[0 0 0 0 0 1 0]} \nonumber\\
  \text{girls} &= \text{[0 0 0 0 0 0 1]} \nonumber
\end{align}

The above techniques treat words as atomic units, i.e., there is no notion of
similarity between words, as these are represented as indices in a vocabulary.
Since, the aim of NLP is to capture the {\em meaning} of words, the above
methods are not very helpful to achieve that. Therefore, many researches have
been conducted to try to effectively represent words as dense continuous vectors
such that vector representations of words having similar meanings are more
closer compared to that of unrelated words.

In 2013, Google AI researcher Tomas Mikolov and co. devised an algorithm to
effectively map words into a continuous vector space of fixed dimensionality,
which is popularly known as {\em word2vec}. It is a kind of unsupervised
learning algorithm with shallow, two-layer neural network models that are
trained to reconstruct linguistic contexts of words. Word2vec takes as its input
a large corpus of text and produces a vector space, typically of several hundred
dimensions, with each unique word in the corpus being assigned a corresponding
vector in the space. Word vectors are learned from the {\em co-occurrence
statistics} of the words in the corpus, i.e., words that share common contexts
in the corpus are located in close proximity to one another in the space.
Word2vec learns word vectors that capture the general meaning of the words from
the unlabeled corpus. But they still need to be adjusted with labeled training
data to be useful for applications such as sentiment analysis, machine
translation, statistical parsing, etc. Therefore, word2vec is carried out as an
{\em unsupervised pre-training} phase followed by {\em supervised training} for
the particular application.

\begin{figure}[h!]\centering
  \includegraphics[width=6in]{fig/country-capital}
  \caption[Country and Capital Vectors Projected by PCA]{Country and Capital
    Vectors Projected by PCA.\cite{phrasevec}
  } \label{fig:country-capital}
\end{figure}

The word representations computed using word2vec are very interesting because
the learned vectors explicitly encode many linguistic regularities and patterns.
Somewhat surprisingly, many of these patterns can be represented as linear
translations. For example, the result of a vector calculation $v$(Madrid) $-$
$v$(Spain) $+$ $v$(France) is closer to $v$(Paris) than to any other word
vector. Similarly, $v(\text{king}) - v(\text{male}) + v(\text{female}) \approx
v(\text{queen})$, and so on.

\begin{figure}[h!] \centering
  \fbox{\includegraphics[width=5in]{fig/word2vec-models}}
  \caption[Word2vec model architectures]{Word2vec model
    architectures.\cite{wordvec}
  } \label{fig:word2vec-models}
\end{figure}

Word2vec can utilize either of two model architectures to produce a distributed
representation of words: {\em continuous bag-of-words} (CBOW) or {\em continuous
skip-gram}. In the continuous bag-of-words architecture, the model predicts the
current word from a window of surrounding context words. The order of context
words does not influence prediction (bag-of-words assumption). In the continuous
skip-gram architecture, the model uses the current word to predict the
surrounding window of context words. The skip-gram architecture weighs nearby
context words more heavily than more distant context words. According to the
authors, CBOW is faster while skip-gram is slower but does a better job for
infrequent words

Here, the skip-gram architecture is briefly described. The training objective of
the skip-gram model is to find word representations that are useful for
predicting the surrounding words in a sentence or a document. More formally,
given a sequence of training words $w_1, w_2, w_3, \ldots, w_T$, the objective
of the skip-gram model is to maximize the average log probability:
\begin{align}
  \frac{1}{T} \sum_{t=1}^{T} \sum_{-c \leq j \leq c, j \neq 0}
  \log p(w_{t+j}|w_t)
\end{align}
where,
\begin{align}
  c &= \text{size of the training context (which can be a function of the center
  word $w_t$)}
  \nonumber
  \\
  T &= \text{size of the corpus (number of words)}
  \nonumber
\end{align}

Larger $c$ results in more training examples and thus can lead to a higher
accuracy, at the expense of the training time. The basic skip-gram formulation
defines $p(w_O|w_I)$, the probability of occurrence of the context word
$o$ given the center word $i$, using the {\em softmax} function:
\begin{align}
  p(w_O|w_I) &=
  \frac{\exp(\bm{u}_{w_O}^T \bm{v}_{w_I})}{\sum_{w=1}^W \exp(\bm{u}_w^T
  \bm{v}_{w_I})}
\end{align}
where $\bm{v}_w$ and $\bm{u}_w$ are the {\em input} and {\em output} vector
representations of the word $w$, and $W$ is the total number of words in the
vocabulary. The final vector representation of the words can be taken as either
the average or the concatenation of the input vector and the output vector of
that word.

However, this function is impractical because the cost of computing
$\nabla \log p(w_O|w_I)$ is proportional to $W$, which is often very large
($10^5$--$10^7$ terms). There are two computationally efficient approximations
to the full softmax.

\subsubsection{Hierarchical Softmax}

The hierarchical softmax uses a binary tree representation of the output layer
with the W words as its leaves and, for each node, explicitly represents the
relative probabilities of its child nodes. These define a random walk that
assigns probabilities to words. The main advantage is that instead of evaluating
W output nodes in the neural network to obtain the probability distribution, it
is needed to evaluate only about $\log_2(W)$ nodes.

More precisely, each word $w$ can be reached by an appropriate path from the
root of the tree. Let $n(w,j)$ be the $j$-th node on the path from the root to
$w$, and let $L(w)$ be the length of this path, so that $n(w,1)=\text{root}$ and
$n(w,L(w)) = w$. In addition, for any inner node $n$, let ch($n$) be an
arbitrary fixed child of $n$ and let $\llbracket x \rrbracket$ be 1 if true and
$-1$ otherwise. Then the hierarchical softmax defines $p(w_O|w_I)$ as:
~
\begin{align}
  p(w_O|w_I) &= \prod_{j=1}^{L(w_O)-1} \sigma \left(
  \llbracket
  n(w_O, j+1) = \text{ch}( n(w_O,j) )
  \rrbracket
  \cdot \bm{u}_{n(w_O,j)}^T \bm{v}_{w_I}
  \right)
\end{align}
where $\sigma(x) = 1 / (1 + e^{-x})$ is the sigmoid function.

\subsubsection{Negative Sampling}

It is an alternative to hierarchical softmax and works on the principle of Noise
Contrastive Estimation (NCE). While NCE can be shown to approximately maximize
the log probability of the softmax, the Skipgram model is only concerned with
learning high-quality vector representations, so NCE can be simplified as
long as the vector representations retain their quality. Let's define define Negative
sampling (NEG) by the objective:
~
\begin{align}
  p(w_O|w_I) &= \log\sigma(\bm{u}_{w_O}^T \bm{v}_{w_I})
  + \sum_{i=0}^k \mathbb{E}_{w_i \sim P_n(w)}
  \left[
    \log\sigma(-\bm{u}_{w_i}^T \bm{v}_{w_I})
  \right]
\end{align}

Thus the task is to distinguish the target word $w_O$ from draws from the noise
distribution $P_n(w)$ using logistic regression, where there are $k$ negative
samples for each data sample. 


\subsubsection{GloVe Word Vectors}

The statistics of word occurrences in a corpus is the primary source of
information available to all unsupervised methods for learning word
representations, and although many such methods now exist, the question still
remains as to how meaning is generated from these statistics, and how the
resulting word vectors might represent that meaning. GloVe (Global Vectors) is a
model for learning word representations that directly captures the global corpus
statistics.\cite{glove} It is an unsupervised learning algorithm, similar to
word2vec, but it has been shown to achieve better performance.

\begin{figure}[!h]\centering
  \includegraphics[width=6in]{fig/glove}
  \caption[Vector mapping of comparative and superlative in GloVe]{Vector
    mapping of comparative and superlative forms of various adjectives in
    GloVe.\cite{glove}}
\end{figure}

The Stanford GloVe project has made high-quality pre-trained word vectors of
various dimensions from 50 to 300 that are trained billions of tokens from
various corpus such as Twitter, Common Crawl, and Wikipedia.
