\section{INTRODUCTION}
\pagenumbering{arabic} % Start arabic page numbers from here

This project, entitled \textbf{"Movie Review Mining and Recommendation
System"}, is a platform where users write reviews of the movies they have
watched and get personalized recommendations of the movies that they have not
watched but which they might be interested in. The recommendations shall be
based on one's history (reviews written by him/her) and reviews given to all the
movies by all the other users of the system. Existing recommendation systems
such as IMDb, Rotten Tomatoes and various e-commerce systems such as Amazon,
eBay, etc. uses rating systems where users give explicit ratings to the
products, and reviews just act as information for other users to read and
compare. One of the main motivation for us to work in this project is the fact
that explicit ratings carry less information than reviews and are, in a way, a
form of information repetition and redundancy. Reviews are more informative
about the product than explicit rating because reviews can be read by other
users and can be voted up/down to help in spam filtering.

As the name suggests, the project has two major components---\emph{review
mining} and \emph{recommendation} system. Review mining (also known as opinion
mining, sentiment analysis) is the process of computationally identifying and
categorizing opinions expressed in a piece of text, especially in order to
determine whether the writer's attitude towards a particular topic, product,
etc. is positive, negative, or neutral. It is an application of natural language
processing (NLP). It involves building a system to collect and categorize
opinions about a product. Since working on open domain of text is too vague and
unfeasible, this project focuses on reviews in movie domain for various reasons.
One reason is the availability of large amount of high quality labeled datasets
of movie reviews on the Internet, and other reason is that in general product
reviews, the features are too diverse. For instance, a phone will have different
features than food items. This diversity brings complications which is not of
primary concern of this project. Similarly, recommendation systems are a
subclass of information filtering system that seek to predict the "rating" or
"preference" that a user would give to an item. They recommend users possible
products they might like based on the products they have liked before and the
products that similar users have liked. In this project, it is targeted to build 
a recommendation system on top of the database of users, movies and ratings
inferred by the sentiment analyzer from the written reviews.

The rest of this document is divided into ten sections, introduction being the
first one. Section 2 talks about the objectives of this project.  Section 3
lists the important research findings. Section 4 shows the data sources and
pre-processing part of the project. Section 5 and 6 describe the sentiment
analysis and recommendation system respectively. Section 7 shows the overall
software development methodology. Section 8 is an important one as it lists the
result of the experiments carried out throughout this project. Section 9
concludes the project and section 10 gives a list of limitations and possible
future enhancements of this project.

\subsection{Background}

Oxford Dictionary defines "review" as "a formal assessment of something with the
intention of instituting change if necessary." So, basically reviews are the
means by which someone expresses their evaluation of something. It may be movie
review, academic review, product review, book review, music review and so on.

With the emerging and developing Web 2.0 that emphasizes the participation of
users more and more in the Internet, websites such as Amazon, Rotten Tomatoes
and IMDb encourage people to post reviews for the products (or movies) they have
used (or seen). These reviews are useful for both information promulgators and
readers. Through movie reviews, on the one hand, filmmakers can gather feedbacks
from the movie fans to further improve the quality of movies. On the other hand,
people could objectively evaluate a movie by viewing other people's opinions,
which will possibly influence their decisions on whether to go and watch the
movie or not. However, many reviews are lengthy with only few sentences
expressing the author's opinions. Therefore, it is hard for people to find or
collect useful information they want. Moreover, for each information unit to be
reviewed, such as a movie, there may be many reviews. If only few reviews are
read, the opinion will be biased. As a result, automatic review mining has
become a hot topic of research recently. Review mining (also known as opinion
mining, sentiment analysis) can be defined as the process of computationally
identifying and categorizing opinions expressed in a piece of text, especially
in order to determine whether the writer's attitude towards a particular topic
is very positive, positive, very negative, negative or neutral.

The huge growth in the amount of available digital information to the Internet
have created a challenge of information overload due to which people are not
able to get timely access to items of interest on the Internet. Recommender
systems are information filtering systems that handles the problem of
information overload by filtering vital information fragment out of large amount
of dynamically generated information according to user's preferences, interest,
or observed behavior about item. The use of recommendation system ensures that
users will get good, personalized and useful recommendation. Moreover, It has
become common for enterprises to collect large volumes of transactional data
that allows for deeper analysis of how a customer base interacts with the space
of product offerings. Recommender systems have evolved to fulfill the natural
dual need of buyers and sellers by automating the generation of recommendations
based on data analysis.

Both the areas of sentiment analysis and recommendation system are under active
research,  intersecting several sub disciplines of statistics, machine learning,
data mining, and information retrievals. Researchers all over the world are
continuously trying to improve the performance of such system with the ultimate
target being the improvement of user experience over the Internet.


\subsection{Problem Statement}

Review systems are becoming increasingly popular with the development of
Internet. Within the last decade, hundreds of small and large review database
and recommendation systems (e.g. IMDb, Yelp, Amazon, etc) have been developed
and millions of products have been extensively reviewed by users from all around
the world. But one thing common in almost all of them is that their
recommendation system is based on explicit ratings given by the users. The
reviews they write only serve as an information for other users to read. It was
realized that explicit rating is not a genuine metric to evaluate a product
because it does not consider all the features of the product. Therefore, rating
(or stars) carry very less information than the descriptive review that the user
gives. Also, the rating and the review may even contradict each other in some
cases. Therefore, rating and review are ought to carry same information, hence,
it is a form of information repetition and redundancy.

So the main motive of doing this project is to eliminate the need for users to
give two entities. The goal is to analyze the movie reviews using natural
language processing and sentiment analysis techniques and extract the opinion of
the user about the movie (whether the review is positive, negative or neutral).
Then this information extracted from reviews (which is more genuine than rating 
star) can be used to know the preferences of users and recommend movies to them
using a hybrid of tag-based and collaborative filtering recommendation
algorithm.

This idea of extracting opinion from descriptive text is still in hot research
phase and is not yet established as a concrete field of study. Therefore,
another motivation of this project is to try to contribute to the research in
this topic.


\subsection{Scope}

The importance of this project is depicted by the sudden explosion of researches
on the topic of sentiment analysis in the last decade. From the simple
statistical bag-of-words model to the current trends in deep learning using
recursive neural networks, it is clear that the community of engineers, computer
scientists and linguists are spending tremendous amount of effort and time
improving the quality of automatic sentiment analysis. This trend can be
justified by the importance of automatic sentiment analysis in this era of
Internet because as the popularity of Internet grows to billions of users, the
number of reviews available for a product can be in the range of thousands (even
millions for a very popular product) and manually analyzing all of them would be
impossible. Also, analyzing only selected few reviews would result in a biased
analysis. Therefore, automatic analysis of reviews is a trending demand of the
business world.

Review mining can be useful in many applications. Online shopping systems are
becoming increasingly popular day by day all over the world. They might possibly
even dominate the trade system of the world in the near future. And those
systems usually require a good recommendation system to increase traffic, and
for their platform to be popular and famous among their competitors. And review
mining is the basic building block for building better recommendation systems.

Also, the product manufacturers can make use of the analysis of the reviews of
their products to know what, in overall, people like and dislike about their
products so that they can decide how to make products that are more liked and
required by the users. This is especially important because product
manufacturers are beginning to realize the effect the reviews can make on the
impression of new users on their products. Therefore, product manufacturers are
constantly trying to get good reviews from the users and automatic review mining
is a must-have tool for them.

Now talking specifically about this project, it focuses on collecting user's
reviews on a movie, converting them into ratings by using sentiment analysis.
Such likes and dislikes of users are then used to provide personalized
recommendation for him/her and other similar users. Currently, a website 
was developed where a user can search for the movies. In order to write reviews,
he/she must be registered. The registration process asks for general user
details. Once registered, the user can login and write reviews. One user cannot
give more than one review for a movie. A user has his own profile page where
he/she gets personalized recommendation of movies. For now, few hundreds of movies 
as well as some users from the real dataset are available online.
A sentiment analysis demo and a recommendation system demo has been created
in the website.
