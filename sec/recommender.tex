\section{RECOMMENDATION SYSTEM}

Recommender systems or Recommendation systems  are a subclass of information
filtering system that seek to predict the "rating" or "preference" that a
user would give to an item. Recommender systems deal with the problem of
information overload by filtering vital information fragment out of large amount
of dynamically generated information according to user's preferences, interest,
or observed behavior about item. Recommender systems have become extremely
common in recent years, and are utilized in a variety of areas: some popular
applications include movies, music, news, books, research articles, search
queries, social tags, and products in general. There are also recommender
systems for experts, collaborators, jokes, restaurants, garments, financial
services, life insurance, romantic partners (online dating), and Twitter pages.

Recommender systems are beneficial to both service providers and users. They
reduce transaction costs of finding and selecting items in an online shopping
environment. Recommendation systems have also proved to improve decision
making process and quality. In e-commerce setting, recommender systems enhance
revenues, for the fact that they are effective means of selling more products.
In scientific libraries, recommender systems support users by allowing them to
move beyond catalog searches. Therefore, the need to use efficient and accurate
recommendation techniques within a system that will provide relevant and
dependable recommendations for users cannot be over-emphasized.

Recommender systems typically produce a list of recommendations in one of two
ways --- through collaborative and content-based filtering or the
personality-based approach. Collaborative filtering approaches build a model
from a user's past behavior (items previously purchased or selected and/or
numerical ratings given to those items) as well as similar decisions made by
other users. This model is then used to predict items (or ratings for items)
that the user may have an interest in. Content-based filtering approaches
utilize a series of discrete characteristics of an item in order to recommend
additional items with similar properties. These approaches are often combined
to form hybrid recommender systems.


\subsection{Phases of Recommendation Process}

The recommendation process generally consists of the following phases, as shown
in Figure \ref{fig:rs-phases}.\cite{rs-review}

\begin{figure}[!h]\centering
  \includegraphics[width=4in]{fig/rs-phases}
  \caption{Recommendation phases}
  \label{fig:rs-phases}
\end{figure}

\subsubsection{Information Collection Phase}

This is the process of collecting relevant information of users to generate a
user profile or model for the prediction tasks including user's attribute,
behaviors or content of the resources the user accesses. A recommendation agent
cannot function accurately until the user profile or model has been well
constructed. The system needs to know as much as possible from the user in order
to provide reasonable recommendation right from the onset. Recommender systems
rely on different types of input such as the most convenient high quality
explicit feedback, which includes explicit input by users regarding their
interest in item or implicit feedback by inferring user preferences indirectly
through observing user behavior. Hybrid feedback can also be obtained through
the combination of both explicit and implicit feedback. In e-learning platform,
a user profile is a collection of personal information associated with a
specific user. This information includes cognitive skills, intellectual
abilities, learning styles, interest, preferences and interaction with the
system. The user profile is normally used to retrieve the needed information to
build up a model of the user.  Thus, a user profile describes a simple user
model. The success of any recommendation system depends largely on its ability
to represent user's current interests. Accurate models are indispensable for
obtaining relevant and accurate recommendations from any prediction techniques.

\begin{enumerate}
  \item {\bf Explicit Feedback:} The system normally prompts the user through
    the system interface to provide ratings for items in order to construct and
    improve his model. The accuracy of recommendation depends on the quantity of
    ratings provided by the user. The only shortcoming of this method is, it
    requires effort from the users and also, users are not always ready to
    supply enough information. Despite the fact that explicit feedback requires
    more effort from user, it is still seen as providing more reliable data,
    since it does not involve extracting preferences from actions, and it also
    provides transparency into the recommendation process that results in a
    slightly higher perceived recommendation quality and more confidence in the
    recommendations.

  \item {\bf Implicit Feedback:} The system automatically infers the user's
    preferences by monitoring the different actions of users such as the history
    of purchases, navigation history, and time spent on some web pages, links
    followed by the user, content of e-mail and button clicks among others.
    Implicit feedback reduces the burden on users by inferring their user's
    preferences from their behavior with the system. The method though does not
    require effort from the user, but it is less accurate.

  \item {\bf Hybrid Feedback:} The strengths of both implicit and explicit
    feedback can be combined in a hybrid system in order to minimize their
    weaknesses and get a best performing system.  This can be achieved by using an
    implicit data as a check on explicit rating or allowing user to give explicit
    feedback only when he chooses to express explicit interest.
\end{enumerate}

\subsubsection{Learning Phase}

It applies a learning algorithm to filter and exploit the user's features from
the feedback gathered in information collection phase. We shall discuss the
algorithms in more detail in following sections.

\subsubsection{Recommendation Phase}

It recommends or predicts what kind of items the user may prefer. This can be
made either directly based on the dataset collected in information collection
phase which could be memory based or model based or through the system's
observed activities of the user. Figure~\ref{fig:rs-phases} highlights the
recommendation phases.


\begin{figure}[!h]\centering
  \includegraphics[width=5in]{fig/rs-techniques}
  \caption{Classification of recommendation techniques}
  \label{fig:rs-techniques}
\end{figure}

\input{sec/similarity}
\input{sec/cbf}
\input{sec/cf}

