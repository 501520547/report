\subsection{Collaborative Filtering}

Collaborative filtering, also referred to as social filtering, filters
information by using the recommendations of other people. It is based on the
idea that people who agreed in their evaluation of certain items in the past are
likely to agree again in the future. A person who wants to see a movie for
example, might ask for recommendations from friends. The recommendations of some
friends who have similar interests are trusted more than recommendations from
others. This information is used in the decision on which movie to
see.\cite{ekstrand}

\begin{figure}[h!]\centering
  \includegraphics[width=5in]{fig/collab-filter}
  \caption{Collaborative filtering}
  \label{fig:collab-filter}
\end{figure}

In the newer, narrower sense, collaborative filtering is a method of making
automatic predictions (filtering) about the interests of a user by collecting
preferences or taste information from many users (collaborating). The underlying
assumption of the collaborative filtering approach is that if a person $A$ has
the same opinion as a person $B$ on an issue, $A$ is more likely to have $B$'s
opinion on a different issue $x$ than to have the opinion on $x$ of a person
chosen randomly.

Collaborative filtering systems have many forms, but many common systems can be
reduced to two steps:
\begin{enumerate}
  \item Look for user who share the same rating patterns with the active user
    (the user whom the prediction is for).
  \item Use the ratings from those like-minded user found in step 1 to calculate
    a prediction for the active user.
\end{enumerate}

Alternatively, item-based collaborative filtering (users who bought $x$ also
bought $y$), proceeds in an item-centric manner:
\begin{enumerate}
  \item Build an item-item matrix determining relationships between pairs of
    item.
  \item Infer the tastes of the current user by examining the matrix and
    matching that user's data
\end{enumerate}

Collaborative filtering is a domain-independent prediction technique for content
that cannot easily and adequately be described by metadata such as movies and
music. Collaborative filtering technique works by building a database (user-item
matrix) of preferences for items by users. It then matches users with relevant
interest and preferences by calculating similarities between their profiles to
make recommendations. Such users build a group called neighborhood. A user gets
recommendations to those items that he has not rated before but that were
already positively rated by users in his neighborhood. The output produced by CF
can be either predictions or list of recommendation. Prediction is a numerical
value, $P_{ij}$, expressing the predicted score of item $j$ for the user $i$,
while Recommendation is a list of top-$N$ items that the user will like the most
as shown in Figure~\ref{fig:cf-process}. The technique of collaborative
filtering can be divided into two categories: memory-based and model-based.

\begin{figure}[h!]\centering
  \includegraphics[width=6in]{fig/cf-process}
  \caption[Collaborative filtering process]{Collaborative filtering
    process.\cite{rs-review}}
  \label{fig:cf-process}
\end{figure}

\subsubsection{Memory-based Techniques}

The items that were already rated by the user before play a relevant role in
searching for a neighbor that shares appreciation with him. Once a neighbor of a
user is found, different algorithms can be used to combine the preferences of
neighbors to generate recommendations. Due to the effectiveness of these
techniques, they have achieved widespread success in real life applications.
Memory-based CF can be achieved in two ways---through user-based and item-based
techniques. User based collaborative filtering technique calculates similarity
between users by comparing their ratings on the same item, and it then computes
the predicted rating for an item by the active user as a weighted average of the
ratings of the item by users similar to the active user where weights are the
similarities of these users with the target item. Item-based filtering
techniques compute predictions using the similarity between items and not the
similarity between users. It builds a model of item similarities by retrieving
all items rated by an active user from the user-item matrix, it determines how
similar the retrieved items are to the target item, then it selects the $k$ most
similar items and their corresponding similarities are also determined.
Prediction is made by taking a weighted average of the active users rating on
the similar items $k$.

\subsubsection{Model-based Techniques}

This technique employs the previous ratings to learn a model in order to improve
the performance of Collaborative filtering Technique. The model building process
can be done using machine learning or data mining techniques. These techniques
can quickly recommend a set of items for the fact that they use pre-computed
model and they have proved to produce recommendation results that are similar to
neighborhood-based recommender techniques. Examples of these techniques include
Dimensionality Reduction technique such as Singular Value Decomposition (SVD),
Matrix Completion Technique, Latent Semantic methods, and Regression and
Clustering.

Model-based techniques analyze the user-item matrix to identify relations
between items; they use these relations to compute the list of top-$N$
recommendations. Model-based techniques resolve the sparsity problems associated
with recommendation systems. Some model-based algorithms for recommendations are
frequent pattern mining, clustering, regression, etc.

\subsubsection{Matrix Completion Techniques}

The essence of matrix completion technique is to predict the unknown values
within the user-item matrices. Correlation based $k$-nearest neighbor ($k$-NN)
is one of the major techniques employed in collaborative filtering
recommendation systems. Different variations of low rank models have been used
in practice for matrix completion especially toward application in collaborative
filtering.

Formally, the task of matrix completion technique is to estimate the entries of
a matrix, $M \in \mathbb{R}^{m \times n}$, when a subset, $\Omega~C\{ (i,j) :
1 \leq i \leq m, 1 \leq j \leq n \}$ of the new entries is observed, a
particular set of row rank matrices, $\hat{M} = UV^T$, where $U \in
\mathbb{R}^{m \times k}$ and $V \in \mathbb{R}^{m \times k}$ and $k \ll
\min(m,n)$. The most widely used algorithm in practice for recovering $M$ from
partially observed matrix using low rank assumption is Alternating Least Square
(ALS) minimization which involves optimizing over $U$ and $V$ in an alternating
manner to minimize the square error over observed entries while keeping other
factors fixed.

\subsubsection{Pros and Cons}

Collaborative Filtering has some major advantages over CBF in that it can
perform in domains where there is not much content associated with items and
where content is difficult for a computer system to analyze (such as opinions
and ideal).  Also, CF technique has the ability to provide serendipitous
recommendations, which means that it can recommend items that are relevant to
the user even without the content being in the user's profile.

Despite the success of CF techniques, their widespread use has revealed some
potential problems such as follows:

\begin{enumerate}

  \item {\bf Cold start problem}: This refers to a situation where a recommender
    does not have adequate information about a user or an item in order to make
    relevant predictions. This is one of the major problems that reduce the
    performance of recommendation system.

  \item {\bf Data sparsity problem}: This is the problem that occurs as a result
    of lack of enough information, that is, when only a few of the total number
    of items available in a database are rated by users. This always leads to a
    sparse user-item matrix, inability to locate successful neighbors and
    finally, the generation of weak recommendations. Also, data sparsity always
    leads to coverage problems, which is the percentage of items in the system
    that recommendations can be made for.

  \item {\bf Scalability}: This is another problem associated with
    recommendation algorithms because computation normally grows linearly with
    the number of users and items. A recommendation technique that is efficient
    when the number of dataset is limited may be unable to generate satisfactory
    number of recommendations when the volume of dataset is increased. Thus, it
    is crucial to apply recommendation techniques which are capable of scaling
    up in a successful manner as the number of dataset in a database increases.
    Methods used for solving scalability problem and speeding up recommendation
    generation are based on dimensionality reduction or offline model
    computation.

\end{enumerate}


\subsection{Hybrid Filtering}

Hybrid filtering technique combines different recommendation techniques in order
to gain better system optimization to avoid some limitations and problems of
individual recommendation algorithms. Some mechanisms of hybridizing various
recommender systems are:

\begin{itemize}[nosep]
  \item{Weighted Hybridization}
  \item{Switching Hybridization}
  \item{Cascade Hybridization}
  \item{Mixed Hybridization}
\end{itemize}

\subsection{Evaluation Metrics}

The quality of a recommendation algorithm can be evaluated using different types
of measurement which can be accuracy or coverage. The type of metric used
depends on the type of filtering technique. Accuracy is the fraction of correct
recommendations out of total possible recommendations while coverage measures
the fraction of objects in the search space the system is able to provide
recommendations for.

\emph{Statistical accuracy metrics} evaluate accuracy of a filtering technique by
comparing the predicted ratings directly with the actual user rating. Mean
Absolute Error (MAE), Root Mean Square Error (RMSE) and Correlation are
usually used as statistical accuracy metrics.

MAE is the most popular and commonly used. It is a measure of deviation of
recommendation from user's specific value. It is computed as follows:
\begin{align}
  \text{MAE} &= \frac{1}{N} \sum_{u,i} \left| p_{u,i} - r_{u,i} \right|
\end{align}
where,
\begin{align}
  p_{u,i} &= \text{predicted rating for user $u$ on item $i$}
  \nonumber\\
  r_{u,i} &= \text{actual rating}
  \nonumber\\
  N &= \text{total number of ratings on the item set}
  \nonumber
\end{align}
The lower the MAE, the more accurately the recommendation engine predicts user
ratings.

Similarly, the Root Mean Square Error (RMSE) is computed as:
\begin{align}
  \text{RMSE} &= \sqrt{\frac{1}{N} \sum_{u,i} ( p_{u,i} - r_{u,i} )^2}
\end{align}
RMSE puts more emphasis on larger absolute error and the lower the RMSE is, the
better the recommendation accuracy.


\subsection{Algorithm}

In this project, modified Alternating Least Square (ALS) algorithm is used to
implement a collaborative recommendation algorithm. The algorithm not only
learns the users' preferences, but also learns the features of movies
simultaneously.\cite{andrew-ng:ml} This algorithm is based on collaborative
filtering approach, but with enough data, can also be used to compute
similarities of movies. This section describes the algorithm used to
implement recommendation system.

Let,
\begin{align}
  n_u &= \text{number of users}
  \nonumber\\
  n_m &= \text{number of movies}
  \nonumber\\
  n &= \text{number of features of a movie}
  \nonumber\\
  m^{(j)} &= \text{number of movies rated by user $j$}
  \nonumber\\
  r(i,j) &= \begin{cases}
    1 & \text{if user $j$ has rated movie $i$}
    \\
    0 & \text{otherwise}
  \end{cases}
  \nonumber\\
  y^{(i,j)} &= \text{rating given by user $j$ to movie $i$}
  \quad \in \{1,2,\ldots,5\}
  \nonumber
\end{align}

It is necessary need to learn parameters:
\begin{align}
  \theta^{(j)} &= \text{preference parameter vector for user $j$}
  &&\in \mathbb{R}^{n}
  \nonumber\\
  x^{(i)} &= \text{feature vector of movie $i$}
  &&\in \mathbb{R}^{n}
  \nonumber
\end{align}
Then, for user $j$ and movie $i$,
\begin{align}
  \text{predicted rating} &= (\theta^{(j)})^T x^{(i)}~\text{stars}
\end{align}

\begin{table}[!h]\centering
  \caption{Sample user-movie matrix}
  \label{tab:user-matrix}
  \renewcommand{\arraystretch}{1.2}
  \begin{tabular}{|c|cccc|}
    \hline
    {\bf Movie} & {\bf User 1} & {\bf User 2} & {\bf User 3} & {\bf User 4}
    \\\hline\hline
    Movie 1 & 5 & 5 & 1 & 1
    \\\hline
    Movie 2 & 5 & ? & ? & 1
    \\\hline
    Movie 3 & ? & 4 & 1 & ?
    \\\hline
    Movie 4 & 1 & 1 & 5 & 4
    \\\hline
    Movie 5 & 1 & 1 & 5 & ?
    \\\hline
  \end{tabular}
\end{table}

Given the movie feature vectors $x^{(i)}$'s, to learn $\theta^{(j)}$, the 
following regularized squared error cost function is minimized:
\begin{align}
  J(\theta^{(j)}) &= \sum_{i:r(i,j)=1}
  \left( (\theta^{(j)})^T x^{(i)} - y^{(i,j)} \right)^2 +
  \frac{\lambda}{2}
  \sum_{k=1}^n \left( \theta^{(j)}_k \right)^2
  %\lVert \theta^{(j)} \rVert_2^2
\end{align}
where $\lambda$ is the L2 regularization parameter.

Similarly, to learn all the user parameters
$\theta^{(1)}, \theta^{(2)}, \ldots, \theta^{(n_u)}$, the cost function is summed
of all users and solve the following optimization problem:
\begin{align}
  \min_{\theta^{(1)}, \ldots, \theta^{(n_u)}}
  J(\theta^{(1)}, \ldots, \theta^{(n_u)})
\end{align}
where,
\begin{align}
  J(\theta^{(1)}, \ldots, \theta^{(n_u)}) &=
  \frac{1}{2} \sum_{j=1}^{n_u} \sum_{i:r(i,j)=1}
  \left( (\theta^{(j)})^T x^{(i)} - y^{(i,j)} \right)^2 +
  \frac{\lambda}{2}
  \sum_{j=1}^{n_u}\sum_{k=1}^n \left( \theta^{(j)}_k \right)^2
\end{align}

Gradient descent can be used to update the parameters $\theta^{(1)},
\theta^{(2)}, \ldots, \theta^{(n_u)}$ as follows:
\begin{align}
  \theta^{(j)}_k &:= \theta^{(j)}_k - \alpha \left[ \sum_{i:r(i,j)=1}
  \left( (\theta^{(j)})^T x^{(i)} - y^{(i,j)} \right) x^{(i)}_k +
  \lambda \theta^{(j)}_k \right]
\end{align}
where $\alpha$ is the learning rate.

Conversely, if given $\theta^{(1)}, \theta^{(2)}, \ldots,
\theta^{(n_u)}$, movie features $x^{(1)}, x^{(2)}, \ldots,
x^{(n_m)}$ can be learned by solving the following optimization problem:
\begin{align}
  \min_{x^{(1)}, \ldots, x^{(n_m)}}
  \frac{1}{2} \sum_{i=1}^{n_m} \sum_{j:r(i,j)=1}
  \left( (\theta^{(j)})^T x^{(i)} - y^{(i,j)} \right)^2 +
  \frac{\lambda}{2}
  \sum_{i=1}^{n_m}\sum_{k=1}^n \left( x^{(i)}_k \right)^2
\end{align}

Actually, both user parameters $\theta^{(1)}$, $\theta^{(2)}$,
\ldots, $\theta^{(n_u)}$ and movie features $x^{(1)}$, $x^{(2)}$, \ldots,
$x^{(n_m)}$ can be learned simultaneously by jointly minimizing the sum of the
two individual cost function.
\begin{align}
  \min_{\substack{\theta^{(1)}, \ldots, \theta^{(n_u)}, \\
  x^{(1)}, \ldots, x^{(n_m)}}}
  J(\theta^{(1)}, \ldots, \theta^{(n_u)}, x^{(1)}, \ldots, x^{(n_m)})
\end{align}
where,
\begin{align}
  J \left(\substack{\theta^{(1)}, \ldots, \theta^{(n_u)},\\
  x^{(1)}, \ldots, x^{(n_m)}} \right) &=
  \frac{1}{2} \sum_{r(i,j)=1}
  \left( (\theta^{(j)})^T x^{(i)} - y^{(i,j)} \right)^2 +
  \frac{\lambda}{2} \left[
    \sum \left( \theta^{(j)}_k \right)^2
    + \sum \left( x^{(i)}_k \right)^2
  \right]
\end{align}

Therefore, out collaborative filtering algorithm can be summarized into
following steps:
\begin{enumerate}

  \item Initialize $\theta^{(1)}$, $\theta^{(2)}$, \ldots, $\theta^{(n_u)}$,
    $x^{(1)}$, $x^{(2)}$, \ldots, $x^{(n_m)}$ to small random values.

  \item Minimize $J(\theta^{(1)}, \ldots, \theta^{(n_u)}, x^{(1)}, \ldots,
    x^{(n_m)})$ using gradient descent (or an advanced optimization algorithm).
    For example,
    \begin{align}
      \theta^{(j)}_k &:= \theta^{(j)}_k - \alpha \left[ \sum_{i:r(i,j)=1}
        \left( (\theta^{(j)})^T x^{(i)} - y^{(i,j)} \right) x^{(i)}_k +
      \lambda \theta^{(j)}_k \right]
      \\[5pt]
      x^{(i)}_k &:= x^{(i)}_k - \alpha \left[ \sum_{j:r(i,j)=1}
        \left( (\theta^{(j)})^T x^{(i)} - y^{(i,j)} \right) \theta^{(j)}_k +
      \lambda x^{(i)}_k \right]
    \end{align}

  \item For a user with parameters $\theta$ and a movie with (learned) features
    $x$, predict a star rating = $\theta^T x$.

\end{enumerate}

\subsubsection{Low Rank Matrix Factorization}

The above algorithm can be made much more efficient by using vectorized
implementation. Actually, the vectorized version of this algorithm is an
application of the low-rank matrix factorization problem. For example, the
user-movie matrix shown in Table \ref{tab:user-matrix} can be presented by the
matrix:
\begin{align}
  Y = \begin{bmatrix}
    5 & 5 & 1 & 1 \\
    5 & ? & ? & 1 \\
    ? & 4 & 1 & ? \\
    1 & 1 & 5 & 4 \\
    1 & 1 & 5 & ?
  \end{bmatrix} \in \mathbb{R}^{n_m \times n_u}
  \nonumber
\end{align}

Similarly, the movie features and user parameters, respectively, can be
represented as:
\begin{align}
  X = \begin{bmatrix}
    \text{---}(x^{(1)})^T\text{---}\\
    \text{---}(x^{(2)})^T\text{---}\\
    \vdots\\
    \text{---}(x^{(n_m)})^T\text{---}\\
  \end{bmatrix} \in \mathbb{R}^{n_m \times n}
  , \quad
  \Theta = \begin{bmatrix}
    \text{---}(\theta^{(1)})^T\text{---}\\
    \text{---}(\theta^{(2)})^T\text{---}\\
    \vdots\\
    \text{---}(\theta^{(n_u)})^T\text{---}\\
  \end{bmatrix} \in \mathbb{R}^{n_u \times n}
\end{align}
such that the prediction matrix, $X\Theta^T \in \mathbb{R}^{n_m \times n_u}$, is
an approximation of the actual rating matrix $Y$.

\subsubsection{Similar Movies}

Since the algorithm also learns feature vector $x^{(i)} \in \mathbb{R}^n$ for
every movie $i$, one can find the $k$ most similar movies to movie $i$ by finding
the $k$ movies $j$ with the smallest $\lVert x^{(i)} - x^{(j)} \rVert$.
