\subsection{Paragraph Vectors}

Many machine learning algorithms require the input to be represented as a
fixed-length feature vector. When it comes to texts, one of the most common
fixed-length features is bag-of-words. Despite their popularity, bag-of-words
features have two major weaknesses: they lose the ordering of the words and they
also ignore semantics of the words. Word vectors can capture some of the
semantics of the words, but to train a sentiment analyzer from the word vectors,
it is required to represent the variable-length review by a fixed-length vectors as
well. One naive way to do that would be to average all the word vectors,
ignoring the word order. The resulting review feature vector can then be fed to
any type of classifier such as SVM, logistic regression, neural network
classifier, etc. Actually, this method was tested and only got about 30\%
accuracy at best. Therefore a better way is necessary to compute the vector
representation of the whole review, which could be arbitrarily long.

One effective way to learn continuous distributed representations of
variable-length pieces of texts, such as sentences, paragraphs, and documents,
is {\em paragraph vector}. It is also an unsupervised learning algorithm very
similar to word2vec, hence, also popularly known as {\em doc2vec}.

In this model, the vector representation is trained to be useful for predicting
words in a paragraph. More precisely, the paragraph vectors are concatenated
with several word vectors from the paragraph and predict the following word in
the given context. Both word vectors and paragraph vectors are trained by the
stochastic gradient descent (SGD) and backpropagation SGD is basically gradient
descent where parameters are updated after every training example or a small
batch of example instead of updating them after checking the whole training set.
While paragraph vectors are unique among paragraphs, the word vectors are
shared. At prediction time, the paragraph vectors are inferred by fixing the
word vectors and training the new paragraph vector until convergence.

In doc2vec, every paragraph is mapped to a unique vector, represented by a
column in matrix D and every word is also mapped to a unique vector, represented
by a column in matrix W. The paragraph vector and word vectors are averaged or
concatenated to predict the next word in a context. The objective is to maximize
the average log probability
\begin{align}
  \frac{1}{T} \sum_{t=k}^{T-k} \log p(w_t | w_{t-k},\ldots,w_{t+k},d)
\end{align}
where $w_1, w_2, \ldots, w_T$ is the sequence of training words, and $k$ is the
context size, and $d$ is the paragraph identifier of that context.

The prediction task is typically done via a multi-class classifier, such as
softmax.
\begin{align}
  p(w_t | w_{t-k},\ldots,w_{t+k},d) &=
  \frac{\exp(y_{w_t})}{\sum_i \exp(y_i)}
\end{align}
where each of $y_i$ is the un-normalized log-probability for each output word
$i$, computed as:
\begin{align}
  y = b + Uh(w_{t-k},\cdots,w_{t+k},d;W,D)
\end{align}
where $U,b$ are softmax parameters, and $h$ is either concatenation or average
function.

The paragraph token can be thought of as another word. It acts as a memory that
remembers what is missing from the current context---or the topic of the
paragraph. For this reason, this model is often called the Distributed Memory
Model of Paragraph Vectors (PV-DM) and it corresponds to the skip-gram model of
word2vec. Another model of paragraph vectors that ignore the order of words is
the Distributed Bag of Words of Paragraph Vector (PV-DBOW), and corresponds to
the CBOW model of word2vec. In PV-DBOW model, no local context is used for the
prediction task. These two models are illustrated in Figure \ref{fig:pv-models}.

\begin{figure}[h!]\centering
  \fbox{
    \begin{subfigure}[t]{0.55\textwidth}\centering
      \includegraphics[width=\textwidth]{fig/pv-dm}
      \caption{PV-DM}
      \label{fig:pv-dm}
    \end{subfigure}
    \begin{subfigure}[t]{0.40\textwidth}\centering
      \includegraphics[width=\textwidth]{fig/pv-dbow}
      \caption{PV-DBOW}
      \label{fig:pv-dm}
    \end{subfigure}
  }
  \caption[Paragraph vector models]{Paragraph vector models.\cite{docembed}}
  \label{fig:pv-models}
\end{figure}

After being trained, the paragraph vectors can be used as
features for the paragraph (e.g., in lieu of or in addition
to bag-of-words). These features can be freed directly to
conventional machine learning techniques such as logistic
regression, SVM, or neural networks. This model was tested for sentiment analysis
and got about 87\% accuracy on binary sentiment classification and about 48\%
accuracy on fine-grained (5-class) sentiment classification task.

The low accuracy is because paragraph vectors only work well when there are lots
of words in paragraph. Generally, movie reviews are short with only few
sentences, so paragraph vectors does not perform well for fine-grained movie
review sentiment classification. Also, paragraph vectors capture the general
{\em topic} of the paragraph, and hence it can be tricked by negations and
idioms which are very common in reviews.
