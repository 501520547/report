\subsection{Deep Learning in NLP}

Sentiment Analysis is a subclass of NLP. Specifically, it is a form of text
classification problem where one classifes a piece of text into one of the 5
sentiment classes. NLP itself is a subclass of Artificial Intelligence (AI) and
Machine Learning. In this age of information, one of the most important topics
in computer science industry is Information Retrieval (IR), and NLP is one of
the most important technologies for IR. Applications of NLP are everywhere
because people communicate almost everything in language: web search,
advertisement, emails, customer service, language translation, radiology
reports, and of course reviews and opinions, and much more.

Until the recent rapid development in neural network technologies, NLP models
were mostly based on lexical-analysis and training any model to do anything
required a lot of manual pre-processing steps including {\em feature
engineering}.  Actually, most of the time and effort of implementing an NLP
application was spent on designing and extracting suitable features of the
datasets, as the performance of the models depend on which features they use.
Since most of the time and effort is spent on manual feature design and
engineering, it can be argued whether this approach can be really called {\em
artificial} intelligence.

The modern approach for NLP is {\em deep learning} using artificial neural
networks (ANN). ANNs are machine learning models inspired from the structure of
the biological brain. An ANN is based on a collection of connected units called
artificial neurons, analogous to axons in a biological brain. Each connection
(synapse) between neurons can transmit a signal to another neuron. The receiving
(postsynaptic) neuron can process the signal(s) and then signal downstream
neurons connected to it. Neurons may have state, generally represented by real
numbers, typically between 0 and 1. Neurons and synapses may also have a weight
that varies as learning proceeds, which can increase or decrease the strength of
the signal that it sends downstream. Further, they may have a threshold such
that only if the aggregate signal is below (or above) that level is the
downstream signal sent.

\begin{figure}[h!]\centering
  \fbox{\includegraphics[width=5in]{fig/neuron}}
  \caption{An artificial neuron.}
  \label{fig:neuron}
\end{figure}

Concretely, an artificial neuron is nothing but a mathematical function that
receives one or more inputs and produces an output, as shown in Figure
\ref{fig:neuron}. It is similar to a logistic regression
unit. Usually the sums of each node are weighted, and the
sum is passed through a nonlinear function known as an activation function or
transfer function. The transfer functions usually have a sigmoid shape, but they
may also take the form of other nonlinear functions, piecewise linear functions,
or step functions. They are also often monotonically increasing, continuous,
differentiable and bounded. Some common activation functions are sigmoid (or
logistic), hyperbolic tan, and rectified linear units (ReLU). An artificial
neuron is very similar to a {\em logistic regression} unit.

Let,
\begin{align}
  n \in \mathbb{Z}^{+} &= \text{number of inputs}
  \nonumber\\
  \bm{x} \in \mathbb{R}^n &= \text{vector of input parameters}
  \nonumber\\
  \bm{w} \in \mathbb{R}^n &= \text{vector of weights associated with the corresponding
  inputs}
  \nonumber\\
  b \in \mathbb{R} &= \text{bias term}
  \nonumber
\end{align}
then the output is,
\begin{align}
  y &= f \left( \sum_{i=1}^n \bm{w}_i \bm{x}_i + b \right)
\end{align}

It can be written vectorically as,
\begin{align}
  y &= f \left( \bm{w} \cdot \bm{x} + b \right)
\end{align}
where $f$ is a monotonically increasing, continuous, differentiable and bounded
non-linear function such as:
\begin{align}
  \text{sigmoid}(x) &= \left( \frac{1}{1 + e^{-x}} \right)
  \\[10pt]
  \tanh(x) &= \left( \frac{1 - e^{-x}}{1 + e^{-x}} \right)
  \\[10pt]
  \text{rectifier}(x) &= x^{+} = \max(0,x)
\end{align}

\begin{figure}[h!] \centering
  \begin{subfigure}[t]{0.32\textwidth} \centering
    \includegraphics[width=\textwidth]{fig/sigmoid}
    \caption{Sigmoid}
    \label{fig:sigmoid}
  \end{subfigure}
  \begin{subfigure}[t]{0.32\textwidth} \centering
    \includegraphics[width=\textwidth]{fig/tanh}
    \caption{tanh}
    \label{fig:tanh}
  \end{subfigure}
  \begin{subfigure}[t]{0.32\textwidth} \centering
    \includegraphics[width=\textwidth]{fig/relu}
    \caption{ReLU}
    \label{fig:relu}
  \end{subfigure}
  \caption[Plots of various nonlinear functions]{Plots of various nonlinear
  functions used as activation functions in artificial neuron}
  \label{fig:nonlinear-plots}
\end{figure}

\begin{figure}[h!] \centering
  \fbox{\includegraphics[width=5in]{fig/ann}}
  \caption[A simple artificial neural network]{A simple artificial neural
  network with 5 input nodes, 1 hidden layer with 3 nodes, and 1 output node.}
  \label{fig:ann}
\end{figure}

Figure \ref{fig:ann} shows a simple neural network. All the arrows shown in that
figure have weights associated with them. Those weights are adjusted during
training phase, which is done using an algorithm called {\em backpropagation}.
Backpropagation is an algorithm to calculate the gradient of the cost function
with respect to the weights in an ANN. It is commonly used as a part of
algorithms that optimize the performance of the network by adjusting the
weights. It is also called backward propagation of errors. The cost function is
a measure of the error of the neural network model. Most common cost functions
are squared errors, cross-entropy error, etc. The neural network weights are
adjusted to minimize the cost function using any of the optimization algorithms
such as gradient descent, BFGS, etc. For instance, in gradient descent, the
weight updating step is:
~
\begin{align}
  \bm{\theta}_i &= \bm{\theta}_i - \alpha \pdv{J(\bm{\theta})}{\bm{\theta}_i}
  \quad \text{ simultaneously for } i = 1, 2, \ldots, n
\end{align}
where,
\begin{align}
  n &= \text{number of parameters to learn}
  \nonumber
  \\
  \bm{\theta} &= \text{parameter vector to learn}
  \nonumber
  \\
  J(\bm{\theta}) &= \text{cost function to minimize}
  \nonumber
  \\
  \alpha &= \text{learning rate}
  \nonumber
\end{align}

The term "deep learning" basically refers to ANN models having many hidden
layers. They could be simple ANNs with multiple hidden layers, or ANNs with
special architectures such as Convolutional Neural Network (CNN), Recurrent
Neural Network, Recursive Neural network (RNN), and so on.

The input layer of an ANN could be the raw input or the initial set of features
to be used. The outputs are usually the probabilities of the data item falling
into one of the output classes. The hidden layers can be interpreted to derive
various combinations of the input features that can learn complex phenomena. The
more hidden layers there are, the more complex phenomena the model can learn,
provided there is sufficient dataset for training.

In deep learning models, there are lots of parameters to learn (adjust) and
usually the training set is not sufficient to learn those parameters with great
certainty. Therefore, deep learning models are very prone to over-fitting. Hence,
regularizing the cost function is vital for good performance of the model. The
most common regularization method is to add the sum of the 2-norm of the
parameter vector multiplied by a regularization parameter to the cost function.

