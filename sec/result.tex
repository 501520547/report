\section{EXPERIMENTS AND RESULTS}

During the research phase, the experiments on different algorithms for 
sentiment analysis were carried out. Basically it was done to compare the accuracy
of these algorithms and select the most feasible one with acceptable accuracy
for the project. Table \ref{tab:results} shows the overview of the experiment
carried out during the research. The experiment was started from a simple
algorithm, Naive Bayes. Scikit-learn library was used for making this process
easier. When trained and tested on eleven thousand sentences from IMDb dataset
for binary class, Naive Bayes gave an accuracy of 83\%. After this, it was tested
on SST for five class sentiments and an accuracy of 40.8\% was obtained at root level.
The state of art for five class(fine grained) sentiment analysis at root level
is 49\% obtained by Tomas Mikolov using doc2vec, so it was necessary to try out other
algorithms as well.

\begin{table}[!h]\centering
  \caption{Experiments carried out during sentiment analysis research}
  \label{tab:results}
  \renewcommand{\arraystretch}{1.5}
  \begin{tabular}{|l|l|c|}
    \hline
    {\bf Method} & {\bf Number of classes} & {\bf Accuracy} \\\hline\hline
    \multirow{2}{*}{Naive Bayes} & Binary & 83\%\\\cline{2-3}
    & 5-class & 40\% \\\hline
    Average word vectors & 5-class & 30\% \\\hline
    Average GloVe vectors & 5-class & 42\% \\\hline
    \multirow{2}{*}{Doc2Vec} & Binary & 87\% \\\cline{2-3}
    & 5-class & 49\% \\\hline
    RNTN & 5-class & 75\% \\\hline
  \end{tabular}
\end{table}

Then neural word vectors was used to compute the review feature (either by
adding or averaging all the word vectors in that review), which in turn was fed
to various classifier models such as logistic regression, softmax, etc. Word vectors from 
two sources were tried. One is by training ourselves, other is the GloVe vectors.

After this, the implementation of doc2vec was tried using gensim library for
binary as well as fine grained sentiments and it was possible able to get an accuracy of
87\% for the former and 49\% for the later one. Doc2vec converts a document into a
paragraph vector which can capture the semantic relationships in the text. 
Logistic regression was applied, linear regression and neural network on this
paragraph vector for the purpose of sentiment analysis. The accuracy obtained
were 48.96\%, 38\%, 49\%, all of them at root level.

Finally, recursive neural tensor network (RNTN) was tried. As the sentence
structures of English language are highly recursive in nature, splitting the
sentences into consecutive noun and verb phrase and applying sentiment analysis
recursively does impressive job. Using RNTN, an accuracy of 75\% was obtained at
phrase level.


\subsection{RNTN Model}

During the RNTN model training, different values of various
hyperparameters were tried to see how they affect the test accuracy of the model. The result
is shown in this section.

\subsubsection{Effect of Batch Size}

Batch size refers to the number of samples that going to be read before updating
the parameter. For instance, let's say there are 1,000 training samples and batch
size is set up to 100. Algorithm takes first 100 samples (from 1st to 100th)
from the training dataset, calculates total cost and gradients, and finally
updates all the parameters. Next it takes second 100 samples
(from 101st to 200th) and train network again.  This procedure is repeated until
all the samples are propagated through the network. Figure \ref{fig:bat-acc}
shows the graph for accuracy plotted against batch size.  With the increase in
batch size the accuracy varies considerably.  The best accuracy was obtained for
both train and test set at batch size equal to 10. The accuracy significantly
decreases from batch size 10 to 20 and then later starts increasing again.
Depending on this graph, the batch size 10 was chosen for training the
RNTN model. For the standard SGD optimization, the default batch size is 1,
i.e., the model parameters are updated after reading every sample. But since 
AdaGrad was used in this project, use decided to settle with batch size of 10.

\begin{figure}[!h]\centering
  \includegraphics[width=5in]{fig/bat-acc}
  \caption{Batch size vs. Accuracy}
  \label{fig:bat-acc}
\end{figure}

\subsubsection{Effect of Regularization}

Regularization is a process of introducing additional information in order to
solve an ill-posed problem or to prevent overfitting. Figure \ref{fig:reg-acc}
shows the graph for accuracy plotted against regularization parameter.With the
increase in the value for regularization, the accuracy initially decreases till
$10^{-4}$ and later starts increasing. For regularization equal to 10 the
accuracy in train set is about 84\% and in train set it is 79\%. Even though the
accuracy seems to be increasing for higher values of regularization, it was realized
that the model is only good at predicting neural sentences. Therefore,
the regularization of $10^{-6}$ was used.

\begin{figure}[!h]\centering
  \includegraphics[width=5in]{fig/reg-acc}
  \caption{Regularization vs. Accuracy}
  \label{fig:reg-acc}
\end{figure}

\subsubsection{Effect of Word Vector Dimension}

Word vector dimension refers to the number of features used to represent a word.
Larger the dimension, better semantic relationships can be captured. Not just
the leaf words, RNTN learns the vector representation (having same
dimensionality) of every phrase and sentence. Figure
\ref{fig:vec-acc} shows the graph for accuracy plotted against word vector
dimension. With the increase in vector dimension, the accuracy fluctuates. The
best accuracy can be observer at dimension between 10 and 20. The accuracy
regarding to vector dimension is almost similar for both train and test set.

\begin{figure}[!h]\centering
  \includegraphics[width=5in]{fig/vec-acc}
  \caption{Vector dimension vs. Accuracy}
  \label{fig:vec-acc}
\end{figure}

\subsubsection{Effect of Epochs}

In neural network terminology, one epoch means one forward pass and one backward
pass of all the training examples. With the increase the in number of epoch, the
cost (loss function) of RNTN model gradually decreases exponentially. Figure
\ref{fig:epo-cost} shows a graph of cost plotted against number of epochs.
Similarly, Figure \ref{fig:epo-acc} shows the effect of number of epochs on
train and test accuracy. The cost is cross entropy function. The number of
iterations per epoch depends on the batch size. Mathematically, number of
iterations per epoch = $n/n_b$, where $n$ is the total number of samples in the
dataset, and $n_b$ is the batch size.

Generally, since the cost function is nonconvex, it is difficult to obtain global optimum
with any certainty. But it is possible to asymptotically reach local optimum with
increasing number of epochs. Usually, there is hardly any improvement after
about 3--5 hours of training.

\begin{figure}[!h]\centering
  \includegraphics[width=5in]{fig/epo-cost}
  \caption{Epochs vs. Cost}
  \label{fig:epo-cost}
\end{figure}

\begin{figure}[!h]\centering
  \includegraphics[width=5in]{fig/epo-acc}
  \caption{Epochs vs. Accuracy}
  \label{fig:epo-acc}
\end{figure}

\subsubsection{Effect of Learning Rate}

The learning rate is how quickly a network abandons old beliefs for new ones.
If the learning rate is too low, the convergence will take too long. And if the
learning rate is too high, the model will oscillate or diverge instead of
converging. Therefore, it is essential to choose the right learning rate for
optimum speed and performance.

In this project, since the AdaGrad optimization algorithm (which is a
modification of SGD) was used, the learning rate changes with time. The learning rate
(per parameter) adapts to the parameter. If the parameter is sparse, its
learning rate increases with time. And if the parameter is dense, its learning
rate decreases with time. But it is necessary to choose a base learning rate.

Different values of the base learning rate were tried to see how they affect the
performance of the model. First it was checked how the cost varies with respect to
the number of epochs for various values of the learning rate. The result is
shown in Figure \ref{fig:learn-cost}. It can be seen from the graph that the optimal
learning rate is between $10^{-2}$ and $10^{-1}$.

Similarly, Figure \ref{fig:learn-acc} shows the effect of learning rate on the
accuracy of the model. This also justifies the fact that the optimal value of
learning rate is between $10^{-2}$ and $10^{-1}$.

\begin{figure}[!h]\centering
  \includegraphics[width=5in]{fig/learn-cost}
  \caption{Epoch vs. Cost for various Learning rates}
  \label{fig:learn-cost}
\end{figure}

\begin{figure}[!h]\centering
  \includegraphics[width=5in]{fig/learn-acc}
  \caption{Learning rate vs. Accuracy}
  \label{fig:learn-acc}
\end{figure}


\subsubsection{Confusion Matrix}

\begin{figure}[!h]\centering
  \includegraphics[width=5in]{fig/confusion}
  \caption{Confusion matrix on test set}
  \label{fig:confusion}
\end{figure}

A confusion matrix is a table that is often used to describe the performance of
a classification model (or "classifier") on a set of test data for which the
true values are known. Rows represent the actual classes and columns represent
the predicted class. The values in the cell are the counts of the model
predicting the class represented by the column for a data whose actual class is
represented by the row.

Figure \ref{fig:confusion} shows the confusion matrix of one of the RNTN models.
The 5 classes are the sentiment classes (0 meaning very negative, 4 meaning very
positive, 2 meaning neutral, and so on). High counts were desired in the diagonal
cells and zero or low counts in the non-diagonal cells. From this confusion
matrix, it can be seen that the number of neutral examples in the dataset is much
greater compared to other classes.


\subsection{Recommender Model}

Just like the RNTN model, the recommender model also has some hyperparameters
that needs tuning. Specifically, the effect of regularization and
feature vector dimension were tested on the performance of the recommender system using
metrics such as cost, root mean square error (RMSE) and mean average error
(MAE).

\subsubsection{Effect of Regularization}

\begin{figure}[!h]\centering
  \includegraphics[width=5in]{fig/reg-v-rmse}
  \caption{Regularization vs. RMSE/MAE}
  \label{fig:reg-v-rmse}
\end{figure}

\begin{figure}[!h]\centering
  \includegraphics[width=5in]{fig/reg-v-cost}
  \caption{Regularization vs. Cost function}
  \label{fig:reg-v-cost}
\end{figure}

Figure \ref{fig:reg-v-rmse} shows the effect or regularization parameter on the
RMSE/MAE metrics, and Figure \ref{fig:reg-v-cost} shows the effect of
regularization parameter on the cost function. The RMSE/MAE graph suggests that
higher regularization is better, but the cost graph suggests otherwise.
Therefore, there is a trade-off between cost and RMSE/MAE, so values of
regularization parameters between 10 and 100 was used for this project.

\subsubsection{Effect of Feature Vector Dimension}

\begin{figure}[!h]\centering
  \includegraphics[width=5in]{fig/feature-v-rmse}
  \caption{Feature vector dimension vs. RMSE/MAE}
  \label{fig:feature-v-rmse}
\end{figure}

\begin{figure}[!h]\centering
  \includegraphics[width=5in]{fig/feature-v-cost}
  \caption{Feature vector dimension vs. Cost function}
  \label{fig:feature-v-cost}
\end{figure}

The recommender system model not only learns users' preferences, but also learns
movie features. The dimension of movie feature vector is a hyperparameter that
must be chosen. For that, the model was tested using various values of feature
vector dimensions. Its effects on RMSE/MAE and cost function are shown in Figure
\ref{fig:feature-v-rmse} and Figure \ref{fig:feature-v-cost} respectively.

It can be seen that both the RMSE/MAE and the cost gradually decrease with
increasing feature vector size, which suggests that it is required to select a very high
value of feature vector size. But it has a pitfall. As the size of feature
vector increases, the time required to train the model increases exponentially.
Keeping that in mind, a feature vector of size between 10 and 20 was chosen as the
trade-off between accuracy and speed.

\subsection{Website}

As an integrated application of the RNTN-based sentiment analysis model and
matrix-factorization-based recommender system, a movie review and
recommendation website was developed using Python/Django. Right now the website has following
features. 

\subsubsection{Features}

\begin{itemize}

  \item Simple authentication system that let's users register in the website
    and login/logout.

  \item A main page that lists the latest movies.

  \item A detail page for every movie that shows the reviews of that movie and
    lists some movies that are {\em similar} to it.

  \item A movie review list page that lists all the reviews of a particular
    movie.

  \item Authenticated users are allowed to write reviews of the movies (if not
    already) as a summary and long descriptive review.

  \item When a review is submitted, the sentiment analyzer automatically
    assigns a predicted rating for it.

  \item A profile page for every user which shows the reviews given by that
    user and list of {\em recommended movies} to him/her.

  \item Admin is allowed to add or modify movies and genres in the database.

  \item A {\em sentiment analysis demo} page where arbitrary sentence is
    analyzed by the sentiment analyzer and the result is shown in a parse tree
    format, with sentiment labels on each node.

  \item A {\em recommendation demo} page where a user is allowed to rate some
    movies and the recommendation algorithm will generate some recommendation
    for him/her.

  \item A search feature to search the movies by name or genre.

  \item A reputation system where users can like or dislike others' reviews so
    that genuine reviews (and users) are encouraged and spam reviews (and users)
    are penalized automatically.
\end{itemize}

The output screenshots are given in Appendix \ref{sec:screenshots}.

