\subsection{Recursive Neural Network (RNN)}

The above mentioned methods do not work very well on sentiment classification
task because they do not consider the compositionality of the review, i.e., they
do not consider how words are combined to form phrases, phrases are combined to
form sentence, and sentences are combined to form paragraphs, and so on. Take
the sentence "A small crowd quietly enters the historical church" as example.
First, break apart the sentence into its respective noun phrase "A small
crowd" and verb phrase "quietly enters the historical church". But this verb
phrase itself can be broken down into a noun phrase and a verb
phrase---"historical church" and "quietly enters".
It can be seen clearly that the syntactic rules of human languages are highly {\em
recursive}.

Now the task is to take a sentence and represent it as
a vector in the same semantic space as the words themselves. So
that phrases like "I went to the mall yesterday", "We went shopping
last week", and "They went to the store", would all be pretty close
in distance to each other. Well ways to train unigram word vectors 
have been checked. Maybe it is good to train bi-gram vectors, trigram vectors and so on, but
this approach has two major issues:
\begin{enumerate}[nosep]
  \item There are literally an infinite amount of possible combinations of
    words. Storing and training an infinite amount of vectors would just be
    absurd.
  \item Some combinations of words while they might be completely reasonable to
    hear in language, may never be represented in the training/dev corpus. So those
    are never learned.
\end{enumerate}

Let's first discuss the difference between semantic and grammatical
understanding of a sentence. Semantic analysis is an understanding of the
meaning of a sentence, being able to represent the phrase as a vector in a
structured semantic space, where similar sentences are very nearby, and
unrelated sentences are very far away.  The grammatical understanding is one
where it is required to identy the underlying grammatical structure of the sentence,
which part of the sentence depends on which other part, what words are modifying
what other words, etc. The output of such an understanding is usually
represented as a parse tree as displayed in Figure~\ref{fig:parsetree}.

\begin{figure}[h!]\centering
  \fbox{\includegraphics[width=4in]{fig/parsetree}}
  \caption{A sentence parse tree}
  \label{fig:parsetree}
\end{figure}

A recursive neural network (RNN) is a kind of deep neural network created by
applying the same set of weights recursively over a structure, to produce a
structured prediction over variable-size input structures, or a scalar
prediction on it, by traversing a given structure in topological order. RNNs
have been successful for instance in learning sequence and tree structures in
natural language processing, mainly phrase and sentence continuous
representations based on word embedding. RNNs can compute compositional vector
representations for phrases of variable length and syntactic type. These
representations can then be used as features to classify each phrase.

\begin{figure}[h!]\centering
  \fbox{\includegraphics[width=2in]{fig/rnn}}
  \caption[Approach of RNN]{Approach of Recursive Neural Network models for
    sentiment: Compute parent vectors in a bottom up fashion using a
    compositionality function $g$ and use node vectors as features for a
    classifier at that node. This function varies for the different
    models.\cite{richi}
  } \label{fig:rnn}
\end{figure}

Figure~\ref{fig:rnn} displays this approach. When an $n$-gram is given to the
compositional models, it is parsed into a binary tree and each leaf node,
corresponding to a word, is represented as a vector. Recursive neural models
will then compute parent vectors in a bottom up fashion using different types of
compositionality functions $g$. The parent vectors are again given as features
to a classifier. For ease of exposition, the trigram of
Figure~\ref{fig:rnn} will be used as an example in further discussions. The section first describe
the operations that the below recursive neural models have in common: word
vector representations and classification. This is followed by descriptions of
two types of RNNs.

Each word is represented as a $d$-dimensional vector.  All word vectors are
initialized by randomly sampling each value from a uniform distribution:
$\mathcal{U}(-r, r)$, where $r = 0.0001$. All the word vectors are stacked in
the word embedding matrix $L \in \mathbb{R}^{d \times |V|}$, where $|V|$ is the
size of the vocabulary. Initially the word vectors will be random but the $L$
matrix is seen as a parameter that is trained jointly with the compositionality
models.

The word vectors can be immediately used as parameters to optimize and as feature
inputs to a softmax classifier. For classification into five classes, the posterior probability 
can be computed over labels given the word vector via:
\begin{align}
  y^a &= \text{softmax}(W_s a + b_s) \in \mathbb{R}^5
  \label{eq:ya}
\end{align}
where
\begin{align}
  W_s &=
  \text{sentiment classification weight matrix}
  && \in \mathbb{R}^{5 \times d}
  \nonumber
  \\
  b_s &= \text{classification bias parameter}
  && \in \mathbb{R}^5
  \nonumber
\end{align}
The softmax function  is a generalization of the logistic function that
"squashes" a $K$-dimensional vector $z$  of
arbitrary real values to a $K$-dimensional vector $\sigma(z)$ of real values in
the range $[0,1]$ that add up to 1. The function is given by:
\begin{align}
  \sigma(z)_j &= \frac{\exp(z_j)}{\sum_{k=1}^K \exp(z_k)} \quad
  \text{for} j = 1,2,\ldots,k
\end{align}

For the
given trigram, this is repeated for vectors $b$ and $c$. The main task of RNN is
to compute the hidden vectors $p_i \in \mathbb{R}^d$ in a bottom-up fashion.

In the standard RNN, first it is determined which parent already has all its
children computed. In the above tree example, $p_1$ has its two children's
vectors since both are words. RNNs use the following equations to compute the
parent vectors:
\begin{align}
  p_1 &= f \left( W \begin{bmatrix} b \\ c\end{bmatrix}\right)
  \\[10pt]
  p_2 &= f \left( W \begin{bmatrix} a \\ p_1\end{bmatrix}\right)
\end{align}
where
\begin{align}
  f=\tanh &= \text{standard element-wise nonlinearity}
  \nonumber\\
  W &= \text{standard RNN weight matrix} && \in \mathbb{R}^{d \times 2d}
  \nonumber
  %b &= \text{standard RNN bias vector} && \in \mathbb{R}^d
  %\nonumber
\end{align}

The bias is omitted for simplicity. The bias can be added as an extra column to $W$
if an additional 1 is added to the concatenation of the input vectors. The
parent vectors must be of the same dimensionality to be recursively compatible
and be used as input to the next composition. Each parent vector $p_i$ is given
to the same softmax classifier of Equation \eqref{eq:ya} to compute its label
probabilities.


\subsection{Recursive Neural Tensor Network (RNTN)}

Most of the parameters are associated with words and each composition function
that computes vectors for longer phrases depends on the actual words being
combined, i.e., the parent vector not only depends on how the composition
parameters interact with the child vectors, but only depend on how the child
vectors interact with each other. In the standard RNN, the input
vectors only implicitly interact through the nonlinearity
(squashing) function., therefore, it cannot learn complex inter-word
relationships such as negations ("not good"), extremizations ("very bad"),
and so on. 

A more direct, possibly multiplicative, interaction would allow the model to
have greater interactions between the input vectors. And that is where Recursive
Neural \emph{Tensor} Network (RNTN) comes into play. The main idea is to use
the same, tensor-based composition function for all nodes. Tensors are geometric
objects that describe linear relations between geometric vectors, scalars, and
other tensors. For example, vectors are order-1 tensors, matrices are order-2
tensors, and so on.

The RNTN uses this definition for computing $p_1$ and $p_2$ (omitting the bias
for simplicity):
\begin{align}
  p_1 &= f \left( 
    \begin{bmatrix} b \\ c \end{bmatrix}^T
    V^{[1:d]}
    \begin{bmatrix} b \\ c \end{bmatrix}
    +
    W \begin{bmatrix} b \\ c \end{bmatrix} 
  \right)
  \\
  p_2 &= f \left( 
    \begin{bmatrix} a \\ p_1 \end{bmatrix}^T
    V^{[1:d]}
    \begin{bmatrix} a \\ p_1 \end{bmatrix}
    +
    W \begin{bmatrix} a \\ p_1 \end{bmatrix}
  \right)
\end{align}
where,
\begin{align}
  W &= \text{standard RNN parameter matrix} &&\in \mathbb{R}^{d \times 2d}
  \nonumber\\
  %b &= \text{standard RNN bias vector} &&\in \mathbb{R}^{d}
  %\nonumber\\
  V &= \text{RNTN tensor parameter} &&\in \mathbb{R}^{d \times 2d \times 2d}
  \nonumber\\
  a, b, c &= \text{leaf word vectors} &&\in \mathbb{R}^d
  \nonumber\\
  p_1, p_2 &= \text{hidden node vectors} &&\in \mathbb{R}^d
  \nonumber
\end{align}

\begin{figure}[h!]\centering
  \includegraphics[width=3in]{fig/rntn-layer}
  \caption[A single layer of RNTN.]{A single layer of RNTN\@. Each dashed box
    represents one of $d$-many slices and can capture a type of influence a
    child can have on its parent.\cite{richi}
  } \label{fig:rntn-layer}
\end{figure}

The main advantage over the previous RNN model, which is a special case of the
RNTN when $V=0$, is that the tensor can directly relate input vectors.
Intuitively, each slice of the tensor can be interpreted as capturing a specific
type of composition


\subsubsection{Tensor Backpropagation through Structure}

RNTN, like any other neural network models, is trained with the backpropagation
algorithm. This section describes how to perform backpropagation through
the tree structure and train the model.

As mentioned above, each node has a softmax classifier trained on its vector
representation to predict a given ground truth or target vector $t$. It is assumed
that the target distribution vector at each node has a 0-1 encoding. That is, if
there are $C$ classes, then it has length $C$ and a 1 at the correct label. All
other entries are 0.

It is required to maximize the probability of correct prediction, or conversely,
minimize the \emph{cross-entropy} error between the predicted distribution $y^i
\in \mathbb{R}^{C\times 1}$ at node $i$ and the target distribution $t^i \in
\mathbb{R}^{C \times 1}$ at that node. This is equivalent, up to a constant, to
minimizing the KL-divergence between the two distributions. The cross-entropy
error of the RNTN model for a sentence, as a function of the RNTN parameters
$\theta = (V,W,b,W_s,b_s,L)$, is:
\begin{align}
  E(\theta) &= \sum_i \sum_j t^i_j \log y^i_j + \lambda \lVert\theta\rVert^2
\end{align}
where $b \in \mathbb{R}^d$ and $b_s \in
\mathbb{R}^5$ are the bias parameters
of RNTN and softmax classifier respectively

To minimize the above error function, it is necessary to calculate its gradient with
respect to all the model parameters. the trigram of
Figure~\ref{fig:rnn} will be used as example to derive the gradients. The derivative for the
weights of the softmax classifier are standard and sum up from each node's
error. They are given by:
\begin{align}
  \pdv{E^a}{W_s} &= (t^a - y^a) \otimes a
  \\[10pt]
  \pdv{E^a}{b_s} &= (t^a - y^a)
\end{align}
where $\otimes$ is the outer product.

It is defined $x_i \in \mathbb{R}^{d \times 1}$ to be the vector at node $i$. In the
example trigram, the $x^i$'s are $(a,b,c,p_1,p_2)$. Each node backpropagates its
error through to the recursively used weights $V,W,b$. Let $\delta^{i,s} \in
\mathbb{R}^{d \times 1}$ be the softmax error vector at node $i$:
\begin{align}
  \delta^{i,s} &= ( W_s^T (y^i - t^i)) \circ f'(x^i)
\end{align}
where $\circ$ is the Hadamard product between the two vectors and $f'$ is the
element-wise derivative of $f$. In the standard case of using $f = \tanh$, it
can be computed using only $f(x^i)$ as follows:
\begin{align}
  f'(x^i)_{[k]} = 1 - f(x^i)_{[k]}^2
\end{align}

The remaining derivatives can only be computed in a top-down fashion from the
top node through the tree and into the leaf nodes. The full derivative for $V$,
$W$ and $b$ is the sum of the derivatives at each node of the tree.
The complete incoming error messages for a node $i$ is defined as $\delta^{i,com}$. The top
node, in the case $p_2$, only receives errors from the top node's softmax layer.
Hence, $\delta^{p_2,com} = \delta^{p_2,s}$ which can be used to obtain the
standard backpropagation derivative for W as follows:
\begin{align}
  \pdv{E^{p_2}}{W} &= \delta^{p_2,com} \otimes
  \begin{bmatrix}a\\p_1\end{bmatrix}
  \\[5pt]
  \pdv{E^{p_2}}{b} &= \delta^{p_2,com}
\end{align}

For the derivative of each slice $k$ = 1, 2, \ldots, $d$, it is observed:
\begin{align}
  \pdv{E^{p_2}}{V^{[k]}} &= \delta^{p_2,com}_k
  \begin{bmatrix} a \\ p_1 \end{bmatrix}
  \begin{bmatrix} a \\ p_1 \end{bmatrix}^T
\end{align}
where, $\delta^{p_2,com}_k$ is just the $k$'th element of this vector.

Now, the error message can be computed for the two children of $p_2$ as follows:
\begin{align}
  \delta^{p_2,down} &= \left(W^T\delta^{p_2,com} + S\right) \circ
  f'\left( \begin{bmatrix}a\\p_1\end{bmatrix}\right)
\end{align}
where, it is defined:
\begin{align}
  S &= \sum_{k=1}^d \delta^{p_2,com}_k
  \left( V^{[k]} + (V^{[k]})^T \right)
  \begin{bmatrix} a \\ p_1 \end{bmatrix}
\end{align}

The children of $p_2$ will then each take half of this vector and add their own
softmax error message for the complete $\delta$. In particular:
\begin{align}
  \delta^{p_1,com} &= \delta^{p_1,s} + \delta^{p_2,down}[d+1:2d]
\end{align}
where $\delta^{p_2,down}[d+1:2d]$ indicates that $p_1$ is the \emph{right} child
of $p_2$ and hence takes the second half of the error. For the final word vector
derivative for $a$, it will be:
\begin{align}
  \delta^{a,com} &= \delta^{a,s} + \delta^{p_2,down}[1:d]
\end{align}

The full derivative for slice $V^{[k]}$ for this trigram tree then is the sum at
each node:
\begin{align}
  \pdv{E}{V^{[k]}} &= \pdv{E^{p_2}}{V^{[k]}} + \delta^{p_1,com}_k
  \begin{bmatrix} b \\ c \end{bmatrix}
  \begin{bmatrix} b \\ c \end{bmatrix}^T
\end{align}
and similarly for $W$.

For this nonconvex optimization, \emph{AdaGrad} is used which converges in less
than 3 hours to a local optimum. AdaGrad (short for "adaptive gradient") is a
modified stochastic gradient descent (SGD) algorithm with \emph{per-parameter}
learning rate, first published in 2011.\cite{adagrad} Informally, it increases
the learning rate for more sparse parameters and decreases the learning rate for
less sparse ones. This strategy often improves convergence performance over
standard SGD in settings where data is sparse and sparse parameters are more
informative. Examples of such applications include natural language processing
and image recognition. It still has a base learning rate $\eta$, but this is
multiplied with the elements of a vector $\{G_{j,j}\}$, which is the diagonal of
the outer product matrix:
\begin{align}
  G &= \sum_{\tau = 1}^t g_{\tau} g_{\tau}^T
\end{align}
where $g_\tau = \nabla Q_i (w)$ is the gradient at iteration $\tau$. The
diagonal is given by:
\begin{align}
  G_{j,j} &= \sum_{\tau = 1}^t g_{\tau,j}^2
\end{align}

This vector is updated after every iteration. The formula for an update is now:
\begin{align}
  w &= w - \eta~\text{diag}(G)^{-\frac{1}{2}} \circ g
\end{align}
or, written as per-parameter updates:
\begin{align}
  w_j &= w_j - \frac{\eta}{\sqrt{G_{j,j}}} \circ g
\end{align}

Each $\{ G_{(i,i)} \}$ gives rise to a scaling factor for the learning rate that
applies to a single parameter $w_i$. Since the denominator in this factor,
$\sqrt{G_i} = \sqrt{\sum_{\tau=1}^t g_\tau^2}$ is the $\ell_2$-norm of previous
derivatives, extreme parameter updates get damped, while parameters that get few
or small updates receive higher learning rates. While designed for convex
problems, AdaGrad has been successfully applied to non-convex optimization.

\newpage
\subsubsection{Some Examples of RNTN Sentiment Classification}

\begin{figure}[h!]\centering
  \fbox{\includegraphics[width=4in]{fig/x-but-y}}
  \caption[Example of correct prediction for contrastive conjunction
  $X~but~Y$]{Example of correct prediction for contrastive conjunction
    $X~but~Y$.\cite{richi}}
  \label{fig:x-but-y}
\end{figure}

\begin{figure}[h!]\centering
  \fbox{\includegraphics[width=\textwidth]{fig/rntn-predictions}}
  \caption[Examples of RNTN predictions for negated sentences]{RNTN prediction
    of positive and negative (bottom right) sentences and their
    negation.\cite{richi}}
  \label{fig:rntn-predictions}
\end{figure}

