\section{DATA SOURCES AND PRE-PROCESSING}

Data is the fuel of any software. To bring the software to life, a ready-made dataset is 
needed for the algorithms to work on. Although the project plans to
develop a system where users can write review themselves, for the initial
training of the models, proper labeled dataset is necessary. And after some
research, few good data sources of movie reviews were found.

\subsection{IMDb Large Movie Review Dataset}

It is a collection of polarity labeled movie reviews extracted from the IMDb
archive. It contains 25,000 highly polar movie reviews for training, and extra
25,000 reviews for testing. It also contains additional 50,000 unlabeled reviews
that might be useful for unsupervised training.\cite{imdb-dataset}.

This is one of the biggest labeled movie review datasets available and hundreds
of researches on sentiment analysis and NLP have been carried out that use this
dataset. But one of the main problems with this dataset is that it only contains
highly polar reviews for training and testing, i.e., the reviews in this dataset
are either very positive, or very negative. There are no slightly positive,
slightly negative, or neutral movie reviews. So this dataset, while being very
useful for binary sentiment classification tasks, is skewed and not very
effective for the fine-grained sentiment classification task like in this project where it 
is required to classify a review into a rating scale from 1 to 5.

\subsection{Cornell Movie Review Data}

Another source of review sentiment data contains 1000 positive and 1000 negative
movie reviews extracted from the IMDb archive.\cite{panglee2005} This is a
relatively small dataset which is available in various versions. One of them is
the sentiment polarity dataset which contains only positive and negative labeled
reviews.  Another is the sentiment scale dataset which is a collection of
documents whose labels come from a rating scale. This dataset can be used for
binary or 4-scale sentiment classification tasks.

\subsection{Stanford Sentiment Treebank (SST)}

This is a relatively new dataset of movie reviews that contains 11,855
one-sentence movie reviews extracted from Rotten Tomatoes website. This dataset
is a little bit special because it not only contains fine-grained sentiment
labels for the whole sentences, but also contains sentiment labels for every
phrase in the parse tree of the sentences. From the parse trees of 11,855
sentences, there are in total 215,154 labeled phrases in the dataset. Not only
that, each phrase in the dataset has been labeled by at least three human
annotators independently. And to train sentiment classifiers based on recursive
neural network models, this is probably the best dataset there is till now. This
dataset can be obtained labeled text format or in Penn Treebank (PTB)
format.\cite{richi}

\begin{figure}[h!]\centering
  \fbox{\includegraphics[width=6in]{fig/treebank}}
  \caption[A sample sentence from the Stanford Sentiment Treebank.]{A sample
    sentence from the Stanford Sentiment Treebank.\cite{richi}}
  \label{fig:treebank}
\end{figure}

\subsection{MovieLens Rating Dataset}

This is a dataset to train and test recommendation system algorithms. It was
collected from the MovieLens website by the GroupLens Research Project at the
University of Minnesota. This dataset consists of 100,000 ratings of 1--5 scale
from 943 users on 1682 movies. Moreover, each user in the dataset has rated at
least 20 movies. It also contains some simple features of the movies like genres
and release dates, as well as some simple demographic information for the users
such as age, gender, occupation and ZIP code.\cite{ml-dataset}

\subsection{Data Split}

All the above datasets come pre-split into train and test sets. Even if the
dataset is not split, it is necessary split it into at least two sets, for training
and testing. This is because testing the performance of a model on the same
dataset that is used to train the model does not give the true indication of how
good the model is performing. In particular, if the system is tested only on the train set,
it is difficult to decide whether the model is overfitting the data or not. Overfitting
the data means that the model performs very well on the train set but performs
very bad in examples not present in the train set. This could be a result of
having too many parameters to learn compared to the size of the train set. And
this problem is usually solved by adding regularization parameters to the cost
function so that the learned parameters do not carry large values. Generally
the model is trained from the train set and tune the model hyperparameters in such a
way that the model that has the best result in the test set is selected, even
though it may not have the best result in the train set.

Actually, even having separate train and test set is not enough. When the
test set is used to decide the best model, it is being assumed that the real data will be
similar to the test set. But this is surely not the case. To simulate testing on
real data, it is necessary to split the dataset into three sets --- train,
validation, and test sets. The basic steps of evaluating model is as follows:

\begin{enumerate}[nosep]
  \item Train the model on the train set.
  \item Test the model on the validation set.
  \item Select the model that performs best on validation set.
  \item Finally, test the model on the test set to get an idea of how it will
    perform on unseen data.
\end{enumerate}

In this way, it is possible to both validate the performance of the model as well as see
how it performs on unseen data, which is an approximation of the real data.

Among the above mentioned datasets, only the SST comes pre-split into the three
sets for training, validating and testing.

\subsection{Pre-processing}

Data pre-processing is an important step in data mining process. It basically
refers to the process of making the collected data suitable to be used for
analysis without any problems and errors. The phrase "garbage in, garbage out"
is particularly applicable to data mining and machine learning projects.
Data-gathering methods are often loosely controlled, resulting in out-of-range
values (e.g., Income: -100), impossible data combinations (e.g., Sex: Male,
Pregnant: Yes), missing values, etc. Analyzing data that has not been carefully
screened for such problems can produce misleading results. Thus, the
representation and quality of data is first and foremost before running an
analysis. Data pre-processing includes cleaning, Instance selection,
normalization, transformation, feature extraction and selection, etc. The
product of data pre-processing is the final training set.

While the aforementioned datasets have already been pre-processed up to a
certain degree, they still cannot be used directly for the application. Below,
some of the pre-processing steps are described.

\subsubsection{Data Cleaning}

Data cleaning (or cleansing) is the process of detecting and correcting (or
removing) corrupt or inaccurate records from a record set, table, or database
and refers to identifying incomplete, incorrect, inaccurate or irrelevant parts
of the data and then replacing, modifying, or deleting the dirty or coarse data.
Data cleansing may be performed interactively with data wrangling tools, or as
batch processing through scripting. All the datasets were already cleaned, so it was
not necessary to do this step.

\subsubsection{Dataset Reduction}

Dataset reduction (or dataset condensation, or instance selection) is an
important data pre-processing step that can be applied in many machine learning
(or data mining) tasks. Approaches for reduction can be applied for reducing the
original dataset to a manageable volume, leading to a reduction of the
computational resources that are necessary for performing the learning process.
This step is especially useful for testing the operation and efficiency of
machine learning (or data mining) algorithms. Sentiment analyzer and recommender 
system models were tested in reduced datasets first to make the computation
process faster.

\subsubsection{Normalization}

Data normalization is the process of converting the data to any kind of
canonical form. In terms of machine learning or signal processing, data
normalization refers to transforming the values to a limited range or scale. For
example, if some movie review dataset provides sentiment labels as rating scales
from 1 to 10, it is required to normalize that to convert the labels to the required
value that is 1 to 5. In this case, a simple division by 2 seems to be
sufficient but in other cases, it might take more operations such as subtracting
mean value and then dividing by the standard deviation.

\subsubsection{Data Transformation}

In computing, a data transformation converts a set of data values from the data
format of a source data system into the data format of a destination data
system. For instance, the Stanford Sentiment Treebank is available in PTB
format, which is represented as a tree string where nodes are separated by
parenthesis. But to use that dataset in the program, it is necessary to convert the tree
string into a real tree data structure.

\subsubsection{Constituency Parsing}

Parsing, syntax analysis or syntactic analysis is the process of analysing a
string of symbols, either in natural language or in computer languages,
conforming to the rules of a formal grammar. The term parsing comes from Latin
\emph{pars} (\emph{orationis}), meaning part (of speech).  Within computational
linguistics the term is used to refer to the formal analysis by a computer of a
sentence or other string of words into its constituents, resulting in a parse
tree showing their syntactic relation to each other, which may also contain
semantic and other information.

\begin{figure}[!h]\centering
  \fbox{\includegraphics[width=2.5in]{fig/constituency-parse-tree}}
  \caption{Constituency-based parse tree}
  \label{fig:constituency-parse-tree}
\end{figure}

Constituency-based parse tree, such as the one show in
Figure~\ref{fig:constituency-parse-tree}, is the entire structure, starting from
\verb=S= and ending in each of the leaf nodes (John, hit, the, ball). The
following abbreviations are used in the tree:
\begin{itemize}
  \item \verb=S= for sentence, the top-level structure in this example
  \item \verb=NP= for noun phrase. The first (leftmost) \verb=NP=, a single noun
    "John", serves as the subject of the sentence. The second one is the object
    of the sentence.
  \item \verb=VP= for verb phrase, which serves as the predicate
  \item \verb=V= for verb. In this case, it's a transitive verb hit.
  \item \verb=D= for determiner, in this instance the definite article "the"
  \item \verb=N= for noun
  \item and so on \ldots
\end{itemize}

In this project, the Recursive Neural Tensor Network (RNTN) model for sentiment analysis has been implemented, 
which requires sentences to be fed in parse tree format. Stanford's free parser software has been used for this purpose.
