\section{SENTIMENT ANALYSIS}


Sentiment analysis (also known as opinion mining and review mining) refers to
the use of natural language processing, text analysis and computational
linguistics to identify and extract subjective information in source materials.
Sentiment analysis is widely applied to reviews and social media for a variety
of applications, ranging from marketing to customer service. This project
involves extracting opinion information from movie reviews, which is basically
the job of sentiment analysis. Therefore, it is necessary to implement some algorithms
to analyze the sentiment of the review as accurately as possible.

\begin{table}[h!] \centering
  \caption{Examples of tricky sentences to analyze the sentiment.}
  \label{tab:sentiment-examples}
  \renewcommand{\arraystretch}{1.5}
  \begin{tabular}{|r|l|c|}
    % heading
    \hline
    {\bf S.N.} & {\bf Sentence} & {\bf Sentiment}
    \\\hline\hline
    1 & It is not bad. & Neutral \\\hline
    2 & It was neither that funny, nor too witty. & Negative \\\hline
    3 & I felt really glad when the movie ended. & Negative \\\hline
  \end{tabular}
\end{table}

Consider the examples in Table \ref{tab:sentiment-examples}. The first sentence
shows the effect of negation. If individual words are checked in that sentence,
the word "bad" suggests that the sentence carries negative sentiment, but the
appearance of the word "not" before the "bad" inverts the effect of "bad"
and therefore the sentence is not negative. In the second sentence, the phrases
"funny" and "too witty" are positive words but the sentence is in overall
negative due to the "neither \ldots nor" structure. The third sentence is an
example of a sarcastic sentence. It says "felt really glad", which carries a
positive sentiment, and does not contain any negations. Still the sentence is
negative as a whole.

While the examples above are easy for a human to judge the sentiment, they are
very tricky and confusing for a computer, as human languages are very ambiguous
and computers do not understand ambiguous language. This shows that automatic
sentiment analysis is a difficult task and hundreds of researches have been
carried out on this topic. Below, some popular methods of implementing sentiment 
analysis that was tried for this project are described.

In simple terms, the sentiment analyzer can be thought of as a black box which
takes movie reviews as input and outputs the corresponding predicted sentiment
rating in the range 1-5, as shown in Figure \ref{fig:blackbox}.

\begin{figure}[h!] \centering
  \includegraphics[width=4in]{fig/blackbox}
  \caption{Black box view of sentiment analyzer}
  \label{fig:blackbox}
\end{figure}

Now that sentiment analyzer black box can be implemented in many different ways.
Existing approaches to sentiment analysis can be grouped into three main
categories: knowledge-based techniques, statistical methods, and deep learning
approaches.

\subsection{Knowledge-based Approach}

Knowledge-based techniques look for unambiguous affect words such as "good",
"bad", "boring", etc to determine the sentiment polarity of the text. This is
probably the simplest way of implementing a sentiment analyzer but this is also
probably the least accurate because, as in the examples shown earlier, reviews
contain contains complex grammatical structures like negations, idioms, and
sarcasms that cannot be all listed explicitly to develop a knowledge-based
sentiment classifier. Therefore, knowledge-based approach only works for simple
naive sentences.

\subsection{Statistical Bag-of-words Methods}

Statistical methods use elements of machine learning such as latent semantic
analysis, support vector machines (SVM), bag-of-words (BOW), etc to calculate
the probabilistic polarity of a text. This model is similar to the
knowledge-based approach in that it also looks at words lexically in isolation,
i.e., it analyzes the character composition of words and not the true meaning of
the word. For example, lexically, the words "swam" is more similar to "swan" and
"swimming".

The only difference between this BOW method and the knowledge-based approach is
that in this method, the knowledge is learned from the label dataset whereas in
the knowledge-based approach, the fact that word "good" carries positive
sentiment and the word "bad" carries negative sentiment is explicitly fed to the
algorithm. In the BOW method, the sentiment of the word "good" is learned from
the statistics of its appearance in the labeled dataset. For example, if there
are 100 negative examples and 100 positive examples in the dataset and 60
positive examples contain the word "good" whereas 20 negative examples contain
the word "good", then the model learns that the word "good" must be 75\%
positive because out of the 80 training examples containing the word "good", 60
are positive and 20 are negative, and therefore, positivity = 60/80 = 0.75.

The list of all the words or tokens contained in the training dataset or corpora
is referred to as the {\em vocabulary} of the dataset or corpora. Once the
statistics for all the words in the vocabulary has been learned from the
training dataset, the probabilities are then fed to a Naive Bayes (NB)
classifier that can predict the degree of positivity or negativity of every word
or token in the vocabulary.  Once the tokens are done, the sentiment of the
whole review can be calculated by first tokenizing the review, removing the
stopwords, and then summing or averaging the positivity and negativity of every
token.

This method is simple to understand and has surprisingly impressive accuracy
considering the fact that it does not capture the semantic meaning of the words
and it only works on probabilities. In fact, what is described above is the
so-called {\em unigram} BOW model, where single words are checked in an isolated
way. Thus, the order of the words in the sentence is ignored and important
information is lost. If a pair of consecutive words is taken from the dataset and
calculate the statistics for them, the {\em bigram} BOW model is obtained.
Similarly, if three consecutive words are taken, the {\em trigram} BOW
model is obtained. By using these {\em n-gram} BOW models, the accuracy of the model can be
boosted further, but it requires more and more computer memory and processor
resources.

\input{sec/deep-learning}

\input{sec/wordvec}

\input{sec/docvec}

\input{sec/rntn}
